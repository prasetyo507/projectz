<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Items extends MX_Controller
{
	public function index()
	{
		$data['items'] = $this->model_crud->getdata('tbl_items', 'id', 'desc');
		$data['page'] = 'v_items';
		$this->load->view('template/template', $data);
	}
	public function add()
	{
		$data['page'] = 'v_additem';
		$this->load->view('template/template', $data);
	}
	public function submit()
	{

		$datetime = date('YmdHis');

		$code_number = preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('code'));
		//qr code
		$this->load->library('ciqrcode');
		$params['data'] = base_url() . 'warehouse/add/' . $this->input->post('code');
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH . 'qrcode/' . $code_number . '_' . $datetime . '.png';
		$this->ciqrcode->generate($params);
		//end qr code

		$data = array(
			'code'			=> $this->input->post('code'),
			'name'			=> $this->input->post('name'),
			'type'			=> $this->input->post('type'),
			'qrcode'		=> $code_number . '_' . $datetime . '.png',
			'created_at'		=> date("Y-m-d H:i:s")
		);
		$this->model_crud->insertdata('tbl_items', $data);

		echo "<script>alert('Success');
		document.location.href='" . base_url() . "items';</script>";
	}
}
