<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History extends MX_Controller
{
	public function index()
	{
		$data['page'] = 'v_history';
		$this->load->view('template/template', $data);
	}
}
