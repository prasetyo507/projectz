<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Items <small>add new item</small></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <a href="<?php echo base_url(); ?>items" class="btn btn-warning btn-sm"><i class="fa fa-angle-left"></i> Back</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="<?php echo base_url() ?>items/submit" enctype="multipart/form-data">
                        <label>Name :</label>
                        <input type="text" class="form-control" name="name" required />
                        <label>Type :</label>
                        <input type="text" class="form-control" name="type" required />
                        </br>
                        <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>