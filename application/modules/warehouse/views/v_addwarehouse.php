<div class="">
    <div class="page-title">
        <div class="title_center">
            <h3><i class="fa fa-cube"></i> <?php echo $item->name ?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12 ">
            <div class="x_panel">
                <div class="x_content">
                    <form method="post" action="<?php echo base_url() ?>warehouse/submit" enctype="multipart/form-data">
                        <label>Item :</label>
                        <p>
                            In :
                            <input class="option" type="radio" class="flat" name="option" value="in" required />
                            Out :
                            <input class="option" type="radio" class="flat" name="option" value="out" required />
                        </p>
                        <label>Total :</label>
                        <input type="number" class="form-control" name="qty" value="0" required />
                        <div class="destination">
                            <br />
                            <label>destination :</label>
                            <select class="form-control" name="destination" required>
                                <option value="dt">Distributor</option>
                                <option value="sls">Sales</option>
                            </select>
                        </div>
                        <input type="hidden" name="code" value="<?php echo $item->code ?>" />
                        </br>
                        <button class="btn btn-success pull-right"><i class="fa fa-save"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".destination").hide();
        $("input[type='radio']").click(function() {
            if ($(this).attr("value") == "in") {
                $(".destination").hide();
            }
            if ($(this).attr("value") == "out") {
                $(".destination").show();

            }
        });

    });
</script>