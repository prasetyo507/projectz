<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse extends MX_Controller
{
	public function index()
	{
		$data['page'] = 'v_warehouse';
		$this->load->view('template/template', $data);
	}
	public function add($id)
	{
		$data['item'] = $this->model_crud->getDetail('tbl_items', 'code', $id)->row();
		$data['page'] = 'v_addwarehouse';
		$this->load->view('template/android', $data);
	}
	public function submit()
	{

		$data = array(
			'code_item'			=> $this->input->post('code'),
			'option'			=> $this->input->post('option'),
			'qty'				=> $this->input->post('qty'),
			'destination'		=> $this->input->post('destination'),
			'created_at'		=> date("Y-m-d H:i:s")
		);
		$this->model_crud->insertdata('tbl_card_stock', $data);

		echo "<script>alert('Success');
		document.location.href='" . base_url() . "scan';</script>";
	}
}
