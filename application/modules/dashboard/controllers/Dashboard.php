<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

	public function index()
	{
		$data['page'] = 'v_dashboard';
		$this->load->view('template/template', $data);
	}
}
