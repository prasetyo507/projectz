<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Scan QRCode</title>
    <link href="<?php echo base_url() ?>assets/webcodecamjs-master/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/webcodecamjs-master/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container" id="QR-Code">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="navbar-form navbar-left">
                    <h4>WebCodeCamJS.js Demonstration</h4>
                </div>
                <div class="navbar-form navbar-right">
                    <select class="form-control" id="camera-select"></select>
                </div>
            </div>
            <hr>
            <div class="panel-body text-center">
                <div id="scan">
                    <div class="well" style="position: relative;display: block;">
                        <canvas id="webcodecam-canvas"></canvas>
                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                    </div>
                </div>
                <div class="thumbnail" id="result">
                    <div class="well" style="overflow: hidden;">
                        <img width="200" height="200" id="scanned-img" src="">
                    </div>
                    <div class="caption">
                        <h3>Scanned result</h3>
                        <a id="scanned-QR"></a>
                    </div>
                    <button type="button" class="btn btn-sm btn-primary" id="rescan">Re-scan</button>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <ul></ul>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/webcodecamjs-master/js/qrcodelib.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/webcodecamjs-master/js/webcodecamjs.js"></script>
    <script type="text/javascript">
        document.getElementById("scan").style.display = "block";
        document.getElementById("result").style.display = "none";

        document.getElementById("rescan").addEventListener("click", function() {
            document.getElementById("scan").style.display = "block";
            document.getElementById("result").style.display = "none";
        });

        var args = {
            resultFunction: function(res) {
                document.getElementById("scan").style.display = "none";
                document.getElementById("result").style.display = "block";
                document.getElementById("scanned-img").src = res.imgData;
                document.getElementById("scanned-QR").href = res.code;
                document.getElementById("scanned-QR").text = res.code;
            }
        };

        var decoder = new WebCodeCamJS("#webcodecam-canvas").buildSelectMenu("#camera-select", "environment|back").init(args);
        decoder.play();

        document.querySelector("#camera-select").addEventListener("change", function() {
            if (decoder.isInitialized()) {
                decoder.stop().play();
            }
        });
    </script>
</body>

</html>